(* ::Package:: *)

(* ::Subsubsection:: *)
(*Instructions*)


(* ::Text:: *)
(*Run this notebook, then go to Palettes \[FilledRightTriangle] Install Palette... to have it accessible in the future from the Palettes menu.*)


(* ::Subsubsection:: *)
(*Code*)


CreatePalette[
Column[
Table[
Grid[
Partition[
PasteButton[Tooltip[Graphics[{#/.ColorData[\[FormalX],"ColorRules"],Rectangle[{0,0}]},ImageSize->20],#],ColorData[\[FormalX],#],Appearance->"Frameless"]&/@
SortBy[
ColorData[\[FormalX],"ColorRules"][[All,1]],
(10^9 #[[1]] +10^3 #[[2]] +#[[3]]&)[List@@ColorConvert[#/.ColorData[\[FormalX],"ColorRules"],"Hue"]]&
],
20,20,1,{}],
Spacings->{0,0}],
{\[FormalX],{"HTML","Legacy","Crayola"}}],
Spacings->1],
WindowTitle->"Colour Palette for HTML, Legacy, Crayola", ClosingAutoSave->False, Magnification->1];
